/*-- import statements --*/
import React, { Component } from "react";
import * as BABYLON from "@babylonjs/core";
import * as BABYLONJS from "@babylonjs/loaders";
import "@babylonjs/loaders/OBJ/objFileLoader";
import {Inspector} from '@babylonjs/inspector';
import OutlineCustomShader from './OutlineCustomShader.tsx';

//state
interface IState {

}

// props
interface IProps {
	
}

//global variables
var canvas		:	any;
var engine		:	BABYLON.Engine;
var scene		:	BABYLON.Scene;
var hemiLight	:	BABYLON.HemisphericLight;
var camera		:	BABYLON.ArcRotateCamera;
var counter		:	number 	= 0;

//mesh objects
var cube : BABYLON.Mesh;
var sphere : BABYLON.Mesh;
var cylinder : BABYLON.Mesh;
var oldSelectedMesh : BABYLON.AbstractMesh;
var anyMeshSelected : boolean;

var outlineMaterial : BABYLON.ShaderMaterial;

export default class <SceneComponent> extends Component<IProps, IState>{
	//code
	constructor(props: IProps) {
		super(props);

		this.state = {
		};
	};
	
	/*************************************************
	*
	* React Life Cycle method calls immediately after application born. Here Acts as Initialization method
	*
	*************************************************/
	componentDidMount() {
		//code
		counter += 1;
		if (counter === 1) {
			this.initialize();
		}
	};

	/*************************************************
	*
	* React Life Cycle method calls when the state of the component changes.
	*
	*************************************************/
	componentDidUpdate(prevProps : any, prevState : any) {

	};

	/*************************************************
	*
	* Function to initialize everything
	*
	*************************************************/
	initialize = () => {
		//code
		//first clear the console
		console.clear();

		//let axis = new BABYLON.AxesViewer(scene, 15);
		
		canvas = document.getElementById("canvas") as HTMLCanvasElement;
		console.log('Canvas created successfully');

		engine = new BABYLON.Engine(canvas, true, {doNotHandleContextLost:true}, true);
		console.log('Engine initialized succesfully');

		document.addEventListener("keydown", (event) => {
			this.keyDown(event);
		});

		document.addEventListener("keyup", (event) => {
			this.keyUp(event);
		});

		anyMeshSelected = false;

		this.createScene();
		this.renderLoop();
	};

	/*************************************************
	 *
	 * To handle Key Down Event (WndProc)
	 *
	 *************************************************/
	keyDown = (event : any) => {
		//code
		//switch case for Keys
		switch (event.key) {
			case "E":
			case "e":
				console.log("Alpha : ", camera.alpha);
			break;

			case "B":
			case "b":
				console.log("Beta: ", camera.beta);
			break;
		}
	};

	/*************************************************
	 *
	 * To handle Key Up Event (WndProc)
	 *
	 *************************************************/
	keyUp = (event : any) => {
			//code
			//switch case for Keys
			switch (event.key) {
				case "E":
				case "e":
					
				break;
			}
		};

	/*************************************************
	*
	* To create babylonjs scene
	*
	*************************************************/
	createScene = () => {
		//code
		scene = new BABYLON.Scene(engine);
		scene.clearColor = new BABYLON.Color4(0.3, 0.3, 0.3, 1.0); //gray background

		//Inspector.Show(scene, {});

		//camera setup
		this.initializeCamera();

		//Task 1
		//1: Loading multiple overlapped meshes using built-in mesh builder
		this.loadMultiple3DMeshObjects();

		//2. Import custom .obj mesh
		this.loadModel();

		//Task 2
		//1. return mesh object when the mouse hover over in the scene

		//register mouse move listener
		scene.onPointerMove = function() {
			castRay();
		}

		function castRay(){
			//casting ray on to the object
			var ray = scene.createPickingRay(scene.pointerX, scene.pointerY, BABYLON.Matrix.Identity(), camera, false);	

			//if ray intersects with any mesh that's the hit and pick the mesh
			var hit = scene.pickWithRay(ray);

			let selectedMesh : BABYLON.AbstractMesh;
	
			//check hit is valid and mesh is picked oherwise no action on null object
			if (hit?.pickedMesh)
			{
				selectedMesh = (hit.pickedMesh);
	
				//if any mesh was selected earlier
				if(anyMeshSelected)
				{
					//earlier mesh was selected first clear the outline generated material on the earlier mesh, if same mesh is selected then ignore
					if(oldSelectedMesh != selectedMesh)
					{
						oldSelectedMesh.material?.dispose();
						selectedMesh.material = outlineMaterial;
						oldSelectedMesh = selectedMesh;
					}
				}
				else
				{
					oldSelectedMesh = selectedMesh;
					selectedMesh.material = outlineMaterial;
					anyMeshSelected = true;
				}
			}
			else
			{
				//not clicked on mesh, if earlier any mesh is selected clear the outline generation
				if(anyMeshSelected)
				{
					anyMeshSelected = false;
					oldSelectedMesh.material?.dispose();
				}
			}
		};


		//2. shaders to generate outline around the selected mesh object.
		outlineMaterial = this.loadCustomShader();

		hemiLight = new BABYLON.HemisphericLight("hemiLight", new BABYLON.Vector3(0,1,0), scene);
		hemiLight.intensity	= 1;
	};

	/*************************************************
	 *
	 * To set camera object with initial position and angle.
	 *
	 *************************************************/
	initializeCamera = () => {
		camera = new BABYLON.ArcRotateCamera("Camera", -Math.PI / 2, Math.PI / 3, 10, new BABYLON.Vector3(0, 0, 0), scene);
		camera.attachControl();
	};


	/*************************************************
	 *
	 * To Load Custom Shader to generate an outline around the returned mesh object.
	 *
	 *************************************************/
	loadCustomShader = () => {

		let outlineCustomShader = new OutlineCustomShader();

		BABYLON.Effect.ShadersStore['customVertexShader'] = outlineCustomShader.getVertexShader();
		BABYLON.Effect.ShadersStore['customFragmentShader'] = outlineCustomShader.getFragmentShader();

		const shaderMaterial = new BABYLON.ShaderMaterial(
			'shader',
			scene,
			{
			vertex: 'custom',
			fragment: 'custom'
			},
			{
			attributes: ['position', 'normal', 'uv'],
			uniforms: [
				'world',
				'worldView',
				'worldViewProjection',
				'view',
				'projection',
				'u_Time',
				'totalVertices',
				'outlineSpeed',
				'outlineLength'
			]
			}
		);
		
		shaderMaterial.alpha = 0;
		// Set the initial time value
		let initialTime = 0;
		// Register a function to be called before each render
		scene.onBeforeRenderObservable.add(() => {
			// Update the time value
			shaderMaterial.setFloat('u_Time', initialTime);
			// Increment the time for the next frame
			initialTime += scene.getEngine().getDeltaTime() * 0.001; // Convert deltaTime to seconds
		});

		return shaderMaterial;
	};

	/*************************************************
	 *
	 * To load multiple 3D mesh objects using built-in mesh builder
	 *
	 *************************************************/
	loadMultiple3DMeshObjects = async() => {

		//1. Load Cube
		this.loadCubeMesh();

		//2. Load sphere
		this.loadSphereMesh();

		//3. Load cylinder
		this.loadCylinderMesh();
	};

	/*************************************************
	 *
	 * To load Cube 3D mesh objects using built-in mesh builder
	 *
	 *************************************************/
	loadCubeMesh = async() => {
		var columns = 6;
		var rows = 4;
		var cubeFaceColors = new Array(6);

		cubeFaceColors[0] = new BABYLON.Color4(1, 0, 0, 1);   // red
		cubeFaceColors[1] = new BABYLON.Color4(0, 1, 0, 1);   // green
		cubeFaceColors[2] = new BABYLON.Color4(0, 0, 1, 1);   // blue
		cubeFaceColors[3] = new BABYLON.Color4(1, 0, 1, 1);   // 
    	cubeFaceColors[4] = new BABYLON.Color4(0, 1, 1, 1);   // 
		cubeFaceColors[5] = new BABYLON.Color4(1, 1, 0, 1);   // 
		
		cube = BABYLON.MeshBuilder.CreateBox("cube", {height: 1, width: 1, depth: 1, faceColors: cubeFaceColors});
		cube.position = new BABYLON.Vector3(0.8, 0.0, -0.4);
	};

	/*************************************************
	 *
	 * To load Sphere 3D mesh objects using built-in mesh builder
	 *
	 *************************************************/
	loadSphereMesh = async() => {
		//Adding a light
		var light = new BABYLON.PointLight("Omni", new BABYLON.Vector3(0.7, 0.2, 0.7), scene);

		sphere = BABYLON.MeshBuilder.CreateSphere("sphere", {});
		sphere.position = new BABYLON.Vector3(0.0, 0.0, 0.1);

		var material = new BABYLON.StandardMaterial("");
		material.alpha = 1;
		material.diffuseColor = new BABYLON.Color3(1.0, 0.2, 0.7);
		sphere.material = material;
	};

	/*************************************************
	 *
	 * To load Cylinder 3D mesh objects using built-in mesh builder
	 *
	 *************************************************/
	loadCylinderMesh = async() => {
		cylinder = BABYLON.MeshBuilder.CreateCylinder("cylinder", {});
		cylinder.position = new BABYLON.Vector3(-0.6, 0.0, 0.8);

		var material = new BABYLON.StandardMaterial("");
		material.alpha = 1;
		material.diffuseColor = new BABYLON.Color3(0.2, 0.8, 1.0);
		cylinder.material = material;
	};

	/*************************************************
	*
	* To load Model into babylon scene
	*
	*************************************************/
	loadModel = async () => {
		//Adding a light
		var light = new BABYLON.PointLight("modelLight", new BABYLON.Vector3(20, 60, 100), scene);
		
		//load the obj model
		let objModel = BABYLON.SceneLoader.ImportMeshAsync("", "/assets/House/", "villageHouse.obj", scene);
		
		//model loaded successfully
		objModel.then((result) => {
			result.meshes[0].scaling = new BABYLON.Vector3(0.3, 0.3, 0.3);
			result.meshes[0].position = new BABYLON.Vector3(0, 0.9, 2.0);
			result.meshes[0].name = "villageHouse";

			outlineMaterial.setFloat(
				'totalVertices',
				result.meshes[0].getTotalVertices()
			  );
			  outlineMaterial.setFloat('outlineSpeed', 2.0);
			  outlineMaterial.setFloat('outlineLength', 2.0);
		});
				
		// Move the light with the camera
		scene.registerBeforeRender(function () {
			light.position = camera.position;
		});
	};

	/*************************************************
	*
	* To render everything into scene
	*
	*************************************************/
	renderLoop = () => {
		engine.runRenderLoop(()=>{
			scene.render();
			this.update();
		});
	};

	/*************************************************
	 *
	 * To update the state while rendering the scene
	 *
	 *************************************************/
	update = () =>{

	};

	/*************************************************
	*
	* React's Render Method
	*
	*************************************************/
	render() {
		return(
		<div className="App">
			<canvas id = "canvas" />
		</div>
		);
	};
};

