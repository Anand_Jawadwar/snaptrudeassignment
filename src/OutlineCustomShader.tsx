import React, { Component } from "react";

export default class OutlineCustomShader {

    getVertexShader = () => {
        let vertexShader  = `
            precision highp float;
            // Attributes
            in vec3 position;
            in vec2 uv;
            in vec3 normal;
            // Uniforms
            uniform mat4 worldViewProjection;
            // Varying
            out vec2 vUV;
            out vec3 outPos;
            out vec3 outNormal;
            out float vertexID;
            void main(void) {
                outPos = position;
                outNormal = normal;
                vertexID = float(gl_VertexID);
                gl_Position = worldViewProjection * vec4(position, 1.0);
                vUV = uv;
            }
        `;
        
        return vertexShader;
    };

    getFragmentShader = () => {
        let fragmentShader = `
            precision highp float;
            in vec2 vUV;
            in vec3 outPos;
            in vec3 outNormal;
            in float vertexID;
            uniform float u_Time;
            uniform float totalVertices;
            uniform float outlineSpeed;
            uniform float outlineLength;
            void main(void) {
                float speed = outlineSpeed;
                float trailLen = outlineLength;
                vec4 desiredColor = vec4(0, 1, 0.96, 1.0);
                float t = -mod(speed * u_Time, 2.0); // Use 2.0 for a complete cycle over 2 seconds
                float movingVertexID = mod(vertexID + t * totalVertices, totalVertices);
                
                float intensity = movingVertexID / ((trailLen) * (totalVertices));
                if(intensity > 1.0) intensity = 0.0;
                gl_FragColor = intensity * desiredColor;
            }
        `;

        return fragmentShader;
    };
};