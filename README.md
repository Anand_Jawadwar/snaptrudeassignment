#### Created by [Anandkumar Jawadwar](https://www.linkedin.com/in/anandjawadwar/)
##### Date : 09/11/2023

* Refer **Output.png** file to see output of Project.
## Setup
#### Download [Node.js](https://nodejs.org/en/download/).
#### Run following commands:

* To install dependencies (only the first time)
  ``` bash
  npm install
  ```
* To run the local server at localhost:8080
  ``` bash
  npm start
  ```
* To build for production in the directory
  ``` bash
  npm run build
  ```

#### ***** Update *****

  Please see Output.mp4 for the assignment status.
  
##Pending work
1. Right now if mouse is hovered over the mesh, that particular mesh is outlined in different way but not as per the requirement.
2. If mouse is hovered from 1 Mesh to another, material of earlier mesh gets disposed completely, I need to handle that scenario,  

  ### Thank you !